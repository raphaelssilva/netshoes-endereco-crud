package br.com.netshoes.crud.controller.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.netshoes.crud.model.Endereco;
import br.com.netshoes.crud.service.EnderecoService;

@RestController
public class RestEnderecoController {
	private EnderecoService enderecoService;

	@Autowired
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}

	@RequestMapping(value = "/rest/endereco", method = RequestMethod.GET)
	public Object index() throws IOException {

		return this.enderecoService.listar();
	}

	@RequestMapping(value = "/rest/endereco", method = RequestMethod.POST)
	public Object create(@RequestBody  @Validated Endereco endereco, BindingResult bindingResult,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (bindingResult.hasErrors()) {
			response.setStatus(HttpStatus.PRECONDITION_FAILED.value());
			return "Endereco Invalido";
		}
		response.setStatus(HttpStatus.CREATED.value());
		return enderecoService.salvar(endereco);
	}

	@RequestMapping(value = "/rest/endereco/{id}", method = RequestMethod.PUT)
	public Object update(@PathVariable Long id, @RequestBody @Validated Endereco endereco, BindingResult bindingResult,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println(bindingResult.getAllErrors().toString());
		if (bindingResult.hasErrors()) {
			response.setStatus(HttpStatus.PRECONDITION_FAILED.value());
			return "Endereco Invalido";
		}
		endereco.setId(id);
		endereco = enderecoService.editar(endereco);
		if(endereco !=null){
			response.setStatus(HttpStatus.OK.value());
			return endereco;	
		}else{
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			return "Falha ao editar endereco.";
		}
		
	}

	@RequestMapping(value = "/rest/endereco/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable Long id, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Endereco endereco = enderecoService.obter(id);
		if (endereco == null) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return "Endereco não encotrado";
		}
		enderecoService.deletar(id);
		response.setStatus(HttpStatus.OK.value());
		return "Endereco deletado com sucesso";
	}

	@RequestMapping(value = "/rest/endereco/{numero}", method = RequestMethod.GET)
	public Object read(@PathVariable Long numero, HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		Endereco endereco = enderecoService.obter(numero);

		if (endereco == null) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return "Endereco não encotrado";
		}
		response.setStatus(HttpStatus.OK.value());
		return endereco;
	}
}