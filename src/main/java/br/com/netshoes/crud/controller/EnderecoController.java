package br.com.netshoes.crud.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/endereco")
public class EnderecoController {

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

	@RequestMapping(value = "")
	public String index(ModelMap modelMap) {
		return "redirect:endereco/list";
	}

	@RequestMapping(value = "/")
	public String indexB(ModelMap modelMap) {
		return "redirect:list";
	}
	@RequestMapping(value = "/new")
	public String getFormNew(ModelMap modelMap) {
		return "formEndereco";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String getFormEdit(@PathVariable Long id, ModelMap modelMap) {
		modelMap.addAttribute("id", id);
		return "formEndereco";
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(ModelMap modelMap) {
		return "listEndereco";
	}
}
