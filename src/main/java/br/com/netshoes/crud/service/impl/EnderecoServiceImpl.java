package br.com.netshoes.crud.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.netshoes.crud.model.Endereco;
import br.com.netshoes.crud.repository.EnderecoRepository;
import br.com.netshoes.crud.service.EnderecoService;
@Service
public class EnderecoServiceImpl implements EnderecoService {

	EnderecoRepository enderecoRepository;

	@Autowired
	public void setEnderecoRepository(EnderecoRepository enderecoRepository) {
		this.enderecoRepository = enderecoRepository;
	}

	@Override
	public Endereco obter(Long id) {
		return this.enderecoRepository.findOne(id);
	}

	@Override
	public Endereco salvar(Endereco endereco) {
		return this.enderecoRepository.save(endereco);
	}

	@Override
	public Endereco editar(Endereco endereco) {
		Endereco enderecoBanco = this.obter(endereco.getId());
		if (enderecoBanco != null) {
			return this.enderecoRepository.save(endereco);
		}
		return null;
	}

	@Override
	public List<Endereco> listar() {
		return (List<Endereco>) this.enderecoRepository.findAll();
	}

	@Override
	public void deletar(Long id) {
		Endereco enderecoBanco = this.obter(id);
		if (enderecoBanco != null) {
			this.enderecoRepository.delete(enderecoBanco);
		}
	}
}
