package br.com.netshoes.crud.service;

import java.util.List;

import br.com.netshoes.crud.model.Endereco;

public interface EnderecoService {
	Endereco obter(Long id);

	Endereco salvar(Endereco endereco);

	Endereco editar(Endereco endereco);

	List<Endereco> listar();

	void deletar(Long id);

}
