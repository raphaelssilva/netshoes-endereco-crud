package br.com.netshoes.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetshoesEnderecoCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(NetshoesEnderecoCrudApplication.class, args);
    }
}
