package br.com.netshoes.crud.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.netshoes.crud.model.Endereco;

public interface EnderecoRepository extends CrudRepository<Endereco, Long> {
	/*Endereco create(Endereco endereco);

	Endereco update(Endereco endereco);

	List<Endereco> list();

	Endereco get(Long id);
	
	void delete(Endereco endereco);*/
}
