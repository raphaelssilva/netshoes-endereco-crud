var app = angular.module('netshoesApp', [ 'ngResource' ]);
app.config(function($httpProvider) {
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
});
app.controller('enderecoCtrl', function($scope, $location, $http, $resource) {
	$scope.endereco = {};
	$scope.enderecos=[];
	$scope.id = angular.element(document.querySelector("#id")).val()
	
    
    $scope.getList = function(){
    	$http.get("/rest/endereco")
		.success(function(data, status, headers, config) {
			$scope.enderecos = data		
		});			
    }
	
	$scope.get = function(id){
    	$http.get("/rest/endereco/"+id)
		.success(function(data, status, headers, config) {
			$scope.endereco = data		
		});			
    }
    
    $scope.listar = function(){
    	window.location = '/endereco/';
    }
    
    $scope.create = function(){
    	window.location = '/endereco/new';
    }
    
    $scope.update = function(id){
    	window.location = '/endereco/edit/'+id;
    }
    
    $scope.deletar = function(id){
    	if(confirm("Desejá realmente deletar o endereço?")){
    		$http.delete("/rest/endereco/"+id, {responseType:"text", transformResponse: function(data, headers){
                return data;
            }})
    		.success(function(data, status, headers, config) {
    			alert(data);	
    			window.location = '/endereco/list';
    		}).error(function(data, status, headers, config) {
			    alert(data)
			});    		
    	}
    }
    if($scope.id){
    	$scope.get($scope.id);
	}else{
		$scope.getList();
	}

	$scope.salvar = function() {
		$http.post("/rest/endereco", $scope.endereco, {responseType:"text", transformResponse: function(data, headers){
            return data;
        }}).success(
				function(data, status, headers, config) {
					alert("Salvo com sucesso");
					window.location = '/endereco/list'

				}).error(function(data, status, headers, config) {
			alert(data)
		});
	}
	
	$scope.editar = function() {
		$scope.endereco.id = $scope.id;
		$http.put("/rest/endereco/"+$scope.id, $scope.endereco).success(
				function(data, status, headers, config) {
					alert("Endereco editado com sucesso.");
					window.location = '/endereco/list';

				}).error(function(data, status, headers, config) {
			alert(data);
		});
	}

	$scope.buscarCep = function() {
		$http.get("http://localhost:8088/cep/buscar/" + $scope.endereco.cep, {responseType:"text", transformResponse: function(data, headers){
            return data;
        }})
				.success(function(data, status, headers, config) {
					data = JSON.parse(data);
					$scope.endereco.cep = data.numero;
					$scope.endereco.rua = data.logradouro;
					$scope.endereco.bairro = data.bairro;
					$scope.endereco.cidade = data.localidade;
					$scope.endereco.estado = data.uf

				}).error(function(data, status, headers, config) {
					alert(data);
					$scope.endereco = {cep:$scope.endereco.cep}
				});

	}

});