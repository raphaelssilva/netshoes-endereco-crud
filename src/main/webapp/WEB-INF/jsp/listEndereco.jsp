<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<jsp:include page="main.jsp"></jsp:include>
<body>

	<div ng-app="netshoesApp" ng-controller="enderecoCtrl">
		<a href="#" ng-click="create()">Novo endereco</a>
		<table>
			<tr>
				<td>Cep</td>
				<td>Numero</td>
				<td>Ação</td>
			</tr>
			<tr ng-repeat="endereco in enderecos">
				<td ng-bind="endereco.cep"></td>
				<td ng-bind="endereco.numero"></td>
				<td><a href="#" ng-click="update(endereco.id)"> editar </a> <a
					href="#" ng-click="deletar(endereco.id)"> deletar </a></td>
			</tr>
		</table>
	</div>

</body>
</html>