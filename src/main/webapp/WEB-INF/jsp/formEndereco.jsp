<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<jsp:include page="main.jsp"></jsp:include>
<body>

	<div ng-app="netshoesApp" ng-controller="enderecoCtrl">
		<input id="id" type="hidden" value="${id}">
		<form name="form">
		<table>

			<tr>
				<td>Cep</td>
				<td><input type="text" size="8" name="cep" ng-model="endereco.cep"
					required /> <span class="error"
					ng-show="form.cep.$error.required"> Campo não pode ser
						vazio!</span></td>
				<td>
					<button ng-click="buscarCep()">buscar</button>
				</td>
			</tr>
			<tr>
				<td>rua</td>
				<td colspan="2"><input type="text" name="rua" ng-model="endereco.rua"
					required readonly="readonly"/> <span class="error"
					ng-show="form.rua.$error.required"> Campo não pode ser
						vazio!</span></td>
				</td>
			</tr>
			<tr>
				<td>numero</td>
				<td colspan="2"><input type="text" ng-model="endereco.numero"  name="numero"
					required/><span class="error"
					ng-show="form.numero.$error.required"> Campo não pode
						ser vazio!</span></td>

				</td>
			</tr>
			<tr>
				<td>bairro</td>
				<td colspan="2"><input type="text" ng-model="endereco.bairro" readonly="readonly"/></td>
			</tr>
			<tr>
				<td>complemento</td>
				<td colspan="2"><input type="text"
					ng-model="endereco.complemento" /></td>
			</tr>
			<tr>
				<td>cidade</td>
				<td colspan="2"><input type="text" ng-model="endereco.cidade"  name="cidade"
					required readonly="readonly"/> <span class="error"
					ng-show="form.cidade.$error.required"> Campo não pode
						ser vazio!</span></td>
			</tr>
			<tr>
				<td>estado</td>
				<td colspan="2"><input type="text" ng-model="endereco.estado" name="estado"
					required readonly="readonly"/> <span class="error"
					ng-show="form.estado.$error.required"> Campo não pode
						ser vazio!</span></td>
			</tr>

		</table>
		</form>
		<button ng-if="!id" ng-disabled="form.$error.required" ng-click='salvar()'>Salvar</button>
		<button ng-if="id" ng-disabled="form.$error.required" ng-click='editar()'>Editar</button>
	</div>
	<script>
		
	</script>
</body>
</html>
